package ru.t1.kharitonova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.kharitonova.tm.api.endpoint.*;
import ru.t1.kharitonova.tm.api.repository.ICommandRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.command.AbstractCommand;
import ru.t1.kharitonova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kharitonova.tm.exception.system.CommandNotSupportedException;
import ru.t1.kharitonova.tm.repository.CommandRepository;
import ru.t1.kharitonova.tm.service.CommandService;
import ru.t1.kharitonova.tm.service.LoggerService;
import ru.t1.kharitonova.tm.service.PropertyService;
import ru.t1.kharitonova.tm.service.TokenService;
import ru.t1.kharitonova.tm.util.SystemUtil;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.kharitonova.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @NotNull
    @Getter
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    @Getter
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance();

    @NotNull
    @Getter
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance();

    @NotNull
    @Getter
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    @Getter
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    @Getter
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classSet =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classSet) {
            registry(clazz);
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.init();
    }

    public void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }


    public void run(@Nullable String[] args) {
        runArguments(args);
        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                runCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void runArguments(@Nullable final String[] arg) {
        if (arg == null || arg.length == 0) return;
        if (arg[0] == null) return;
        runArgument(arg[0]);
        System.exit(0);
    }

    protected void runCommand(@NotNull final String command) {
        runCommand(command, true);
    }

    protected void runCommand(@NotNull final String command, final boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void runArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

}
