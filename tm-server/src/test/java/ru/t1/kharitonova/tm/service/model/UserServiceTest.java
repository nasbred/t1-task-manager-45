package ru.t1.kharitonova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.model.IProjectService;
import ru.t1.kharitonova.tm.api.service.model.ITaskService;
import ru.t1.kharitonova.tm.api.service.model.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.user.EmailEmptyException;
import ru.t1.kharitonova.tm.exception.user.LoginEmptyException;
import ru.t1.kharitonova.tm.exception.user.PasswordEmptyException;
import ru.t1.kharitonova.tm.exception.user.UserNotFoundException;
import ru.t1.kharitonova.tm.marker.ServerCategory;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.service.ConnectionService;
import ru.t1.kharitonova.tm.service.PropertyService;
import ru.t1.kharitonova.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Category(ServerCategory.class)
public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService, projectService, taskService);

    @NotNull
    private List<User> userList = new ArrayList<>();

    @Before
    public void init() {
        @NotNull final User userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setLocked(true);
        userTest.setRole(Role.USUAL);

        if (userService.findByLogin("admin") == null) {
            userService.add(userAdmin);
            userList.add(userAdmin);
        } else {
            userList.add(userService.findByLogin("admin"));
        }
        if (userService.findByLogin("test") == null) {
            userService.add(userTest);
            userList.add(userTest);
        } else {
            userList.add(userService.findByLogin("test"));
        }
    }

    @After
    public void clear() {
        userService.removeAll();
        userList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final User user = new User();
        user.setLogin("test_add");
        user.setPasswordHash("test_add");
        user.setEmail("test_add@test.ru");
        user.setRole(Role.USUAL);
        userService.add(user);
        Assert.assertEquals(userList.size() + 1, userService.getSize());
    }

    @Test
    public void testAddAll() {
        @NotNull List<User> userDTOS = new ArrayList<>();
        final int count = userList.size();
        for (int i = 0; i < count; i++) {
            @NotNull final User user = new User();
            user.setLogin("test_add_all " + i);
            user.setPasswordHash("test_add_all " + i);
            user.setEmail("test_add@test.ru");
            user.setRole(Role.USUAL);
            userDTOS.add(user);
        }
        userService.addAll(userDTOS);
        Assert.assertEquals(count * 2, userService.getSize());
    }

    @Test
    public void testCreate() {
        @NotNull final String login = "test_create";
        @NotNull final String password = "test_create";
        @NotNull final Role role = Role.USUAL;

        Assert.assertNotNull(userService.create(login, password, role));
        Assert.assertEquals(userList.size() + 1, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateWithNullLogin() {
        userService.create(null, "password");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateWithNullPassword() {
        userService.create("login", null);
    }

    @Test
    public void testFindAll() {
        @Nullable List<User> userList = userService.findAll();
        Assert.assertNotNull(userList);
        Assert.assertEquals(userService.getSize(), userList.size());
    }

    @Test
    public void testFindById() {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @Nullable final User actualUserDTO = userService.findOneById(id);
            Assert.assertNotNull(actualUserDTO);
            Assert.assertEquals(user.getId(), actualUserDTO.getId());
        }
        Assert.assertNull(userService.findOneById("otherId"));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            Assert.assertNotNull(login);
            @Nullable final User actualUser = userService.findByLogin(login);
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(user.getId(), actualUser.getId());
        }
        Assert.assertNull(userService.findByLogin("other_login"));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
    }

    @Test
    public void testFindOneByEmail() {
        @Nullable final User user = userService.findByLogin("test");
        Assert.assertNotNull(user);
        @Nullable final String email = user.getEmail();
        Assert.assertNotNull(email);
        @Nullable final User actualUser = userService.findByEmail(email);
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(user.getId(), actualUser.getId());

        Assert.assertNull(userService.findByEmail("other_email"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
    }

    @Test
    public void testGetSize() {
        final long userSize = userService.getSize();
        Assert.assertEquals(userList.size(), userSize);
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < userList.size(); i++) {
            @NotNull final User user = userList.get(i);
            userService.remove(user);
            Assert.assertEquals(userService.getSize(), userList.size() - i - 1);
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            Assert.assertNotNull(userService.removeByLogin(login));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByLoginNegative() {
        @NotNull final String login = "Other_Login";
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin(login));
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testRemoveByEmailPositive() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            Assert.assertNotNull(userService.removeByEmail(email));
        }
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    public void testRemoveByEmailNegative() {
        @NotNull final String email = "Other_Email";
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByEmail(email));
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testRemoveAll() {
        userService.removeAll();
        Assert.assertEquals(0L, userService.getSize());
    }

    @Test
    public void testSetPassword() {
        @NotNull final String newPassword = "newPassword";
        for (@NotNull final User user : userList) {
            @NotNull final IPropertyService propertyService = new PropertyService();
            @NotNull final String id = user.getId();
            userService.setPassword(id, newPassword);
            @Nullable final User actualUser = userService.findOneById(id);

            Assert.assertNotNull(actualUser);
            Assert.assertEquals(HashUtil.salt(propertyService, newPassword), actualUser.getPasswordHash());
            Assert.assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, "")
            );
            Assert.assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, null)
            );
        }

        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.setPassword(null, newPassword)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.setPassword("", newPassword)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.setPassword("other_Id", newPassword)
        );
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String firstName = "Update First Name";
        @NotNull final String lastName = "Update Last Name";
        @NotNull final String middleName = "Update Middle Name";
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            userService.updateUser(id, firstName, lastName, middleName);
            @Nullable final User actualUser = userService.findOneById(id);
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(firstName, actualUser.getFirstName());
            Assert.assertEquals(lastName, actualUser.getLastName());
            Assert.assertEquals(middleName, actualUser.getMiddleName());
        }
        Assert.assertEquals(userList.size(), (long) userService.getSize());

        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.updateUser(null, firstName, lastName, middleName)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> userService.updateUser("", firstName, lastName, middleName)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.updateUser("otherId", firstName, lastName, middleName)
        );
    }

    @Test
    public void testLockUserByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User actualUserDTO = userService.lockUserByLogin(login);
            Assert.assertNotNull(actualUserDTO);
            Assert.assertTrue(actualUserDTO.isLocked());
        }
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin("random_login"));
    }


    @Test
    public void testUnlockUserByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User actualUserDTO = userService.unlockUserByLogin(login);
            Assert.assertNotNull(actualUserDTO);
            Assert.assertFalse(actualUserDTO.isLocked());
        }
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin("otherLogin"));
    }

}
