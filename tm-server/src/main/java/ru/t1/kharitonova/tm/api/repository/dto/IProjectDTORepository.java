package ru.t1.kharitonova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.model.ProjectDTO;
import ru.t1.kharitonova.tm.enumerated.Status;

import java.util.List;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    void changeStatusById(@Nullable String id, @Nullable Status status);

    boolean existsById(@NotNull final String id);

    @Nullable
    ProjectDTO findOneById(@NotNull String id);

    @Nullable
    ProjectDTO findOneByIdAndUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO findOneByIndex(@NotNull Integer index);

    @Nullable
    ProjectDTO findOneByIndexByUserId(
            @NotNull String user_id,
            @NotNull Integer index
    );

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAllByUserIdWithSort(@NotNull String userId, @NotNull String sort);

    long getSize();

    long getSizeForUser(@NotNull String user_id);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    void removeAllByList(@NotNull List<ProjectDTO> models);

}
