package ru.t1.kharitonova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.model.IProjectRepository;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project>
        implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void changeStatusById(@Nullable final String id, @Nullable final Status status) {
        entityManager
                .createQuery("UPDATE Project e SET status = :status WHERE e.id = :id")
                .setParameter("id", id)
                .setParameter("status", status)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Project findOneByIdAndUserId(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final String jpql = "SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Project findOneByIndex(@NotNull final Integer index) {
        @NotNull final String jpql = "SELECT e FROM Project e";
        return entityManager.createQuery(jpql, Project.class)
                .setMaxResults(index + 1)
                .getResultList()
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Project findOneByIndexByUserId(@NotNull final String user_id, @NotNull final Integer index) {
        if (user_id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final String jpql = "SELECT e FROM Project e WHERE e.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", user_id)
                .setMaxResults(index + 1)
                .getResultList()
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("FROM Project", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserIdWithSort(@NotNull final String user_id, @NotNull final String sort) {
        if (user_id.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT e FROM Project e WHERE e.user.id = :user_id ORDER BY :sort";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("user_id", user_id)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Override
    public long getSize() {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM Project")
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String user_id) {
        return (long) entityManager
                .createQuery("SELECT COUNT(1) FROM Project e WHERE e.user.id = :user_id")
                .setParameter("user_id", user_id)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(Project.class, id));
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeOneByIndexForUser(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndexByUserId(userId, index));
    }

    @Override
    public void removeAll() {
        findAll().forEach(this::remove);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        findAllByUserId(userId).forEach(this::remove);
    }

    @Override
    public void removeAllByList(@NotNull final List<Project> models) {
        models.forEach(this::remove);
    }

}
