package ru.t1.kharitonova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    void changeStatusById(
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@NotNull final String id);

    @Nullable
    Project findOneById(@NotNull String id);

    @Nullable
    Project findOneByIdAndUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull Integer index);

    @Nullable
    Project findOneByIndexByUserId(
            @NotNull String user_id,
            @NotNull Integer index
    );

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserIdWithSort(@NotNull String userId, @NotNull String sort);

    long getSize();

    long getSize(@NotNull String user_id);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    void removeAllByList(@NotNull List<Project> models);

}
