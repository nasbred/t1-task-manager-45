package ru.t1.kharitonova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.repository.model.IAbstractRepository;
import ru.t1.kharitonova.tm.api.service.IConnectionService;
import ru.t1.kharitonova.tm.api.service.model.IAbstractService;
import ru.t1.kharitonova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>>
        implements IAbstractService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract R getRepository(@NotNull final EntityManager entityManager);

    @Override
    public void add(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.addAll(models);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
}
