package ru.t1.kharitonova.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String password;

    public UserChangePasswordRequest(@Nullable final String token) {
        super(token);
    }

    public UserChangePasswordRequest(@Nullable final String token, @Nullable final String password) {
        super(token);
        this.password = password;
    }

}
