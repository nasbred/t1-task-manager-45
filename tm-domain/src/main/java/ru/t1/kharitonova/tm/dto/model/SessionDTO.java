package ru.t1.kharitonova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    @Column(name = "role")
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

    public SessionDTO(@NotNull final String userId, @Nullable final Role role) {
        super(userId);
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionDTO sessionDTO = (SessionDTO) o;
        return created.equals(sessionDTO.created) &&
                role == sessionDTO.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(created, role);
    }

}
