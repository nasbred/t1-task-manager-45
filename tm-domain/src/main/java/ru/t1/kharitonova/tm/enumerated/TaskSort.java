package ru.t1.kharitonova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.comparator.CreatedComparator;
import ru.t1.kharitonova.tm.comparator.NameComparator;
import ru.t1.kharitonova.tm.comparator.StatusComparator;
import ru.t1.kharitonova.tm.dto.model.TaskDTO;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @Getter
    @NotNull
    private final String displayName;

    @Getter
    @NotNull
    private final Comparator<TaskDTO> comparator;

    TaskSort(@NotNull final String displayName, @NotNull final Comparator<TaskDTO> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @Override
    public String toString() {
        switch (this) {
            case BY_NAME:
                return "name";
            case BY_STATUS:
                return "status";
            case BY_CREATED:
                return "created";
            default:
                return "error";
        }
    }

}
