package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class ExistsLoginException extends AbstractException {

    public ExistsLoginException() {
        super("Error! Login already exists.");
    }

}
