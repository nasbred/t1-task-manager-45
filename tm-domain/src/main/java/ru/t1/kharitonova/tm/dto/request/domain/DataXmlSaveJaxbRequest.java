package ru.t1.kharitonova.tm.dto.request.domain;

import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

public final class DataXmlSaveJaxbRequest extends AbstractUserRequest {

    public DataXmlSaveJaxbRequest(@Nullable final String token) {
        super(token);
    }

}
